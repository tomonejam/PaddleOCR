import yaml
import math

import numpy as np
import cv2 as cv
import pandas as pd

from glob import glob
from tqdm import tqdm
from random import shuffle, random, randrange, uniform

from os.path import join, exists

from paddle.io import Dataset
from nist_generator.utils import (
    RandomStringGenerator, StringImageComposer, BackgroundComposer, BiasedNoise, CharSet)

from .imaug import transform, create_operators


class RBoxDataSet(Dataset):

    def __init__(self, config, mode, logger, seed=None):
        super(RBoxDataSet, self).__init__()

        global_config = config['Global']
        dataset_config = config[mode]['dataset']
        loader_config = config[mode]['loader']
        batch_size = loader_config['batch_size_per_card']
        data_dir = dataset_config['data_dir']
        text_anno = dataset_config['text_anno']
        use_bbox = dataset_config['use_bbox']

        self.bbox_resize_ratio = None
        if use_bbox:
            self.bbox_resize_ratio = dataset_config['bbox_resize_ratio']

        self.use_bbox = use_bbox
        self.dataset = self.load_dataset(data_dir, text_anno, use_bbox)
        if loader_config['shuffle']:
            shuffle(self.dataset)

        self.ops = create_operators(
            dataset_config['transforms'], global_config)

        # Configure character synthesis generator
        self.charsyn_ratio = 0
        charsyn_cfg = dataset_config.get('charsyn', None)
        if charsyn_cfg is not None:

            logger.info('Character synthesis is enabled')
            self.charsyn_ratio = charsyn_cfg['ratio']

            self.char_img_composer = StringImageComposer(
                accessor=CharSet,
                normalize=True,
                **charsyn_cfg['image_composer']
            )

            self.char_text_generator = RandomStringGenerator(
                char_set=self.char_img_composer.nist_accessor.supported_char(),
                **charsyn_cfg['text_generator']
            )

            self.char_bg_composer = BackgroundComposer(
                color_range=None,
                erode_size=None,
                **charsyn_cfg['background_composer']
            )

        # Configure handwrite generator
        self.handwrite_ratio = 0
        handwrite_cfg = dataset_config.get('handwrite_generator', None)
        if handwrite_cfg is not None:

            logger.info('Hand written sample generator is enabled')

            self.handwrite_ratio = handwrite_cfg['ratio']
            config = handwrite_cfg['text_generator']
            self.text_generator = RandomStringGenerator(
                char_set=global_config['character_dict_path'],
                **config
            )

            self.img_composer = StringImageComposer(
                **handwrite_cfg['image_composer'])
            self.bg_composer = BackgroundComposer(
                **handwrite_cfg['background_composer'])

        # Configure blending
        self.blend_prob = 0
        blend_cfg = dataset_config.get('blending', None)
        if blend_cfg is not None:

            logger.info('Blending is enabled')
            self.blend_prob = blend_cfg['prob']
            self.blend_intensity = blend_cfg['intensity']
            self.blend_scale = blend_cfg['scale']
            self.blend_translate = blend_cfg['translate']
            self.blend_hvflip_prob = blend_cfg['hvflip_prob']

        # Configure biased noising
        self.noise_prob = 0
        noise_cfg = dataset_config.get('biased_noising', None)
        if noise_cfg is not None:

            logger.info('Biased noising is enabled')
            self.noise_prob = noise_cfg.pop('prob')
            self.noiser = BiasedNoise(**noise_cfg)

        # Dataset length
        self.total_data = int(
            len(self.dataset) * (1 + self.handwrite_ratio + self.charsyn_ratio))

    def load_dataset(self, data_dir, text_anno, use_bbox=False):

        # Load text annotation
        data = pd.read_csv(text_anno)
        file_names = data['filename']
        text_labels = data['label']

        dataset = []
        for file_name, text_label in tqdm(
                zip(file_names, text_labels),
                total=len(file_names),
                desc='Loading annotation into memory'):

            image_path = join(data_dir, file_name + '.jpg')
            rbox_path = image_path + '.mark'

            if not exists(image_path):
                raise FileNotFoundError(image_path)
            if not exists(rbox_path):
                raise FileNotFoundError(rbox_path)

            with open(rbox_path, 'r') as f:
                rbox_anno = yaml.load(f, Loader=yaml.FullLoader)

            dataset.append((image_path, text_label, rbox_anno))

        return dataset

    def pull_sample(self, idx):

        if idx >= len(self.dataset):
            if idx >= (1.0 + self.handwrite_ratio) * len(self.dataset):
                text_anno = self.char_text_generator()
                image = self.char_img_composer(text_anno)
                image = self.char_bg_composer(image)
                image = cv.cvtColor(image, cv.COLOR_GRAY2BGR)
                text_anno = text_anno.replace('_', '')
            else:
                text_anno = self.text_generator()
                image = self.img_composer(text_anno)
                image = self.bg_composer(image)
                image = cv.cvtColor(image, cv.COLOR_GRAY2BGR)

        else:
            image_path, text_anno, rbox_anno = self.dataset[idx]
            rbox_anno = rbox_anno[0]
            image = cv.imread(image_path, cv.IMREAD_COLOR)
            assert image is not None

            rad = math.radians(rbox_anno['degree'])
            if self.use_bbox:

                if isinstance(self.bbox_resize_ratio, list):
                    resize_ratio = uniform(
                        min(self.bbox_resize_ratio), max(self.bbox_resize_ratio))
                else:
                    resize_ratio = self.bbox_resize_ratio

                ctr_x = rbox_anno['x']
                ctr_y = rbox_anno['y']
                box_w = rbox_anno['w'] * resize_ratio
                box_h = rbox_anno['h'] * resize_ratio

                img_w = image.shape[1]
                img_h = image.shape[0]

                matrix = cv.invertAffineTransform(np.float32([
                    [math.cos(rad), math.sin(rad),
                        -ctr_x * math.cos(rad) - ctr_y * math.sin(rad) + ctr_x],
                    [-math.sin(rad), math.cos(rad),
                        -ctr_y * math.cos(rad) + ctr_x * math.sin(rad) + ctr_y]
                ]))

                image = cv.warpAffine(
                    image, matrix, (image.shape[1], image.shape[0]))
                image = image[
                    max(int(ctr_y - box_h / 2), 0):min(int(ctr_y + box_h / 2), img_h),
                    max(int(ctr_x - box_w / 2), 0):min(int(ctr_x + box_w / 2), img_w),
                    :
                ]

            else:
                if math.cos(rad) < 0:
                    image = image[::-1, ::-1, :].copy()

            # Random biased noising
            if self.noise_prob > random():
                image = self.noiser(image)

        return image, text_anno

    def __getitem__(self, idx):

        image, text_anno = self.pull_sample(idx)

        # Blending
        if self.blend_prob > random():

            blend_image, _ = self.pull_sample(randrange(len(self)))

            # Scale
            scale = uniform(min(self.blend_scale), max(self.blend_scale))
            blend_image = cv.resize(blend_image, (0, 0), fx=scale, fy=scale)

            # Intensity
            intensity = uniform(
                min(self.blend_intensity), max(self.blend_intensity))
            blend_image = (
                blend_image * (np.max(image) * intensity / 255.0)).astype(image.dtype)

            # Flipping
            if random() > self.blend_hvflip_prob:
                blend_image = blend_image[::-1, ::-1, ...]

            # Crop or pad for y
            def crop_or_pad(input_image, target_size, dim):

                dims = len(input_image.shape)
                source_size = input_image.shape[dim]
                if source_size > target_size:
                    start = int((source_size - target_size) / 2)
                    end = start + target_size
                    slice_param = [slice(None)] * dims
                    slice_param[dim] = slice(start, end)
                    input_image = input_image[slice_param]
                else:
                    pad_total = target_size - source_size
                    pad_start = int(pad_total / 2)
                    pad_param = [(0, 0)] * dims
                    pad_param[dim] = (pad_start, pad_total - pad_start)
                    input_image = np.pad(input_image, pad_param)

                return input_image

            for dim in [0, 1]:
                blend_image = crop_or_pad(blend_image, image.shape[dim], dim)

            # Translate
            dims = len(blend_image.shape)
            height = blend_image.shape[0]
            width = blend_image.shape[1]

            trans_x = int(blend_image.shape[1] * uniform(
                min(self.blend_translate), max(self.blend_translate)))
            trans_y = int(blend_image.shape[0] * uniform(
                min(self.blend_translate), max(self.blend_translate)))

            pad_param = [(0, 0)] * dims
            pad_param[0] = (abs(trans_y), abs(trans_y))
            pad_param[1] = (abs(trans_x), abs(trans_x))
            blend_image = np.pad(blend_image, pad_param)

            y_shift = abs(trans_y) + trans_y
            x_shift = abs(trans_x) + trans_x
            blend_image = blend_image[
                y_shift:y_shift + height, x_shift:x_shift + width, ...]

            # Blending
            image = np.maximum(image, blend_image)

        # Transform
        image_buffer = cv.imencode('.png', image)[1].tobytes()
        data = {'image': image_buffer, 'label': text_anno}
        outs = transform(data, self.ops)
        if outs is None:
            return self.__getitem__(np.random.randint(self.__len__()))
        return outs

    def __len__(self):
        return self.total_data
